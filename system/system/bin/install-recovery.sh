#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:67108864:e8d91c941fc3bad75bd643d9568c5f560e722a91; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:4c1951cd447c83066cca80dbae1e3ab33f1d9252 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:67108864:e8d91c941fc3bad75bd643d9568c5f560e722a91 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
